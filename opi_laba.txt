#include <iostream> 
#include <ctime> 
using namespace std;
void FillArray(int*, int);
void PrintArray(int*, int);
void FormatTimeFromSecondsToMinutes(int, int &, int &);
void ShowFormattedTime(int, int, int);
void GetMinMax(int*, int, int&, int&);
void GetAverage(int*, int, int&);
int PromtForInpunt();
void GetCount(int, int*, int, int&);
int main()
{
	srand(time(0));
	int size = rand() % 25 + 25;
	cout << "Size = " << size << endl;
	int *mass = new int[size];
	FillArray(mass, size);
	PrintArray(mass, size);

	int minutes = 0, seconds = 0;
	for (int i = 0; i < size; i++)
	{
		int Time = mass[i];
		FormatTimeFromSecondsToMinutes(Time, minutes, seconds);
		ShowFormattedTime(i, minutes, seconds);
	}

	int min = 0, max = 0;
	GetMinMax(mass, size, min, max);
	FormatTimeFromSecondsToMinutes(min, minutes, seconds);
	cout << endl << "The minimal result is 0" << minutes << ":" << seconds << endl;

	FormatTimeFromSecondsToMinutes(max, minutes, seconds);
	cout << "The maximum result is 0" << minutes << ":" << seconds << endl;

	int aver = 0;
	GetAverage(mass, size, aver);
	FormatTimeFromSecondsToMinutes(aver, minutes, seconds);
	cout << "The average result is 0" << minutes << ":" << seconds << endl;

	int Seconds = PromtForInpunt();
	cout << endl;

	int count = 0;
	GetCount(Seconds, mass, size, count);
	cout << endl << "The number of competition results exceeding the entered value is " << count << endl;
	return 0;
}
void FillArray(int *_mass, int _size)
{
	for (int i = 0; i < _size; i++)
		_mass[i] = rand() % 150 + 30;
}
//-------------------------------------------------------� 
void PrintArray(int *_mass, int _size)
{
	for (int i = 0; i < _size; i++)
		cout << _mass[i] << " ";
	cout << endl;
}
//--------------------------------------------------------
void FormatTimeFromSecondsToMinutes(int _Time, int & _minutes, int & _seconds)
{
	_minutes = _Time / 60;
	_seconds = _Time % 60;
}
//---------------------------------------------------------
void ShowFormattedTime(int _ID, int _minutes, int _seconds)
{
	if (_seconds >= 10)
	{
		cout << "Sportsman " << _ID + 1 << ": 0" << _minutes << ":" << _seconds << endl;
	}
	else
	{
		cout << "Sportsman " << _ID + 1 << ": 0" << _minutes << ":0" << _seconds << endl;
	}
}
//---------------------------------------------------------
int PromtForInpunt()
{
	int minutes, seconds;
	bool flag = false;
	do
	{
		cout << "\nEnter time in format MM:CC\n(Remember! You can enter time only from 00:00 to 03:00)\nminutes = ";
		cin >> minutes;
		cout << "seconds = ";
		cin >> seconds;
		if ((minutes < 3) && (minutes >= 0) && (seconds >= 0) && (seconds >= 10) && (seconds < 60))
		{
			cout << "\ntime = 0" << minutes << ":" << seconds;
			flag = true;
		}
		else
		{
			if ((minutes < 3) && (minutes >= 0) && (seconds >= 0) && (seconds < 10))
			{
				cout << "\ntime = 0" << minutes << ":0" << seconds;
				flag = true;
			}
			else
			{
				if ((minutes == 3) && (seconds == 0))
				{
					cout << "\ntime = 0" << minutes << ":0" << seconds;
					flag = true;
				}
				else
				{
					cout << "\nWrong! You need to enter time only from 00:00 to 03:00\nPlease,try again.\n";
				}
			}
		}
	} while (flag != true);
	return minutes * 60 + seconds;
}

void GetMinMax(int *_mass, int _size, int &min, int &max)
{
	min = 181, max = -1;
	for (int i = 0; i < _size; i++)
	{
		if (_mass[i] < min) min = _mass[i];
		if (_mass[i] > max) max = _mass[i];
	}
}
void GetAverage(int *_mass, int _size, int &aver)
{
	int sum = 0;
	for (int i = 0; i < _size; i++)
		sum += _mass[i];
	aver = sum / _size;
}

void GetCount(int time, int *_mass, int _size, int& count)
{
	count = 0;
	for (int i = 0; i < _size; i++)
		if (time < _mass[i]) 
			count++;
}